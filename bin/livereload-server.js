#!/usr/bin/env node


var tinylr = require('tiny-lr');

// standard LiveReload port
var port = process.argv[2] || 35729;
// console.log(process.argv)

// tinylr(opts) => new tinylr.Server(opts);
tinylr()
  .on('GET /changed', function (request, response) {
    // res.write('changed');
    console.log(`changed ${request.method} ${request.url}`);
    // console.log(request.headers);
    response.end();
  })
  .listen(port, function () {
    console.log('... Listening on %s ...', port);
  })