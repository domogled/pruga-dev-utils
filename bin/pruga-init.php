<?php

echo "DEPRECATED";
exit();

$files = ['dev.sh','dev.py', 'dev.php'];

function potvrdit_prepsani($file) {

    $options = ['A', 'n'];
    
    while(True){
        $confirm = readline("Přepsat soubor $file [" . join(" ", $options) ."]: ");

        switch ($confirm) {
            case 'A':
                return True;
            case 'n':                        
                return False;
            default:
                echo "napiš A jako ANO nebo n jako NE\n";
                break;
        }
    }
    return False;
}

foreach ($files as $file) {
    $file_path = getcwd() . DIRECTORY_SEPARATOR . $file;
    $file_url = "https://gitlab.com/domogled/pruga-dev-utils/raw/master/$file";

    if(is_file($file_path) && !potvrdit_prepsani($file)){
        continue;  
    }

    echo "kopíruji $file_url =>  $file_path \n";
    
    $source = file_get_contents($file_url);

    if($source === False){
        echo "CHYBA: Selhalo stažení souboru $file_path\n";
        continue;
    }

    file_put_contents($file_path, $source);

    chmod($file_path, 0775);
   
};
