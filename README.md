# Пруга build system

Toto je nejjednodušší *build system*. Možnosti jsou prakticky neomezené, respektive jsou omezené možnostmi operačního systému.

## Použití:

Pro editor **vscode** jsem vytvořil jednoduché [rozšíření](https://gitlab.com/domogled/pruga-vscode), které nedělá nic víc, než po stisknutí kláves `ctrl+shift+s` uloží aktuální soubor a spustí skript `./dev.sh $nazev_souboru`.

Pro jiné editory si to musíš nějak nastavit. Možná ani nebdue třeba programovat žádné rozšíření. Celá funkčnost spočívá v několika málokrocích. Na libovolnou klávesovou zkratku (já používýám kombinaci `ctrl + shit + S`),případně na nějaké vlastní tlačítko zavěsíme tuto funkčnost.

    - uložit aktuální soubor
    - spustit `./dev.sh` ideálně s parametrem aktuálně uloženého souboru,ale není to nutné,jen přijdeme o tu informaci v našich skriptech

Příjemné je, když se soubor `./dev.sh` spustí v integrovaném terminálu (pokud ho editor má), pak vidíme okamžitě i výstup. Ale nastavit to lze i jinak. Můžeš použít třeba grafický terminál operačního systému,který si spustíš spolu s editorem a vhodně uspořádáš na svém monitoru. 

Ze souboru *dev.sh* lze spouštět různé příkazy, nebo zavolat *dev.py*, či *dev.php*. Můžeš přidat další jazyky a skripty.

## Příklady

Po uložení souboru chci spustit testy.

*./dev.sh*

```shell
#!/usr/bin/env sh

pytest
```

spuštění jen některých testů můžu podle potřeby měnit zakomentováním patřičných řádků.

*./dev.sh*

```shell
#!/usr/bin/env sh

#pytest
pytest ./src/sub1
#pytest ./src/sub2
pytest ./src/sub2/sub21

```

Automatická kompilace projektu může vypadat celkem jednoduše například takto.

*./dev.sh*

```shell
#!/usr/bin/env sh

mkdir -p ./build
cp ./src/index.html ./build/index.html

```

Nevyhovuje ti programování v shellu ? Máš raději Python? Prosím.

*./dev.sh*

```shell
#!/usr/bin/env sh

./dev.py $1

```

*./dev.py*

```python
#!/usr/bin/env python3

import sys

print('PYTHON: Пруга spouští dev.py')
print(sys.argv)

from pathlib import Path

file = Path(sys.argv[0])

if file.suffix == '.html':
    print(f"změnil se html soubor {str(file.resolve())}")
else:
    print("změnil se jiný soubor")

```